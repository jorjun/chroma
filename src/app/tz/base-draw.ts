import { ElementRef } from '@angular/core'
import { Point } from '@lib/matrix'

export interface Options {
    colour: {
        background: string
        fg: string
    }
    opacity?: number
}

/**
 * Canvas Draw base
 */
export class Draw {
    defaultOptions: Options = {
        colour: {
            background: 'red',
            fg: 'black',
        },
    }
    ctx: CanvasRenderingContext2D
    scale = 1
    _cursor: Point = { x: 0, y: 0 }
    private _width?: number
    private _path?: Path2D

    constructor(private canvas: ElementRef<HTMLCanvasElement>, private _options?: Options) {
        this._options = _options ?? this.defaultOptions
        this._options.opacity = _options?.opacity ?? 1
        const ctx = canvas.nativeElement.getContext('2d')
        if (!ctx) throw `Failed to get context from canvas: ${canvas}`
        this.ctx = ctx
    }

    get options() {
        return this._options ?? this.defaultOptions
    }
    set options(options: Options) {
        this._options = options
    }

    get pos() {
        return this._cursor
    }

    get x() {
        return this._cursor.x
    }

    set x(num: number) {
        this._cursor.x = num
    }

    get width() {
        return this._width ?? this.canvas.nativeElement.width
    }

    get path() {
        return this._path ?? new Path2D()
    }
    set path(_path: Path2D) {
        this._path = _path
    }

    get height() {
        return this.canvas.nativeElement.height
    }

    drawText(words: string, pos: Point, colour: string, font: string, opacity = 1.0) {
        const ctx = this.ctx
        ctx.save()
        ctx.font = font
        ctx.textAlign = 'center'
        ctx.fillStyle = colour
        ctx.globalAlpha = opacity
        ctx.fillText(words, pos.x, pos.y)
        ctx.restore()
    }

    clearRect() {
        this.ctx.save()
        this.ctx.fillStyle = this.colour.background
        this.ctx.rect(0, 0, this.width, this.height)
        this.ctx.fill()
        this.ctx.restore()
    }

    get colour() {
        return this._options!.colour
    }

    fill(path: Path2D, pos: Point, fillStyle = 'black', scale: Point = { x: 1, y: 1 }) {
        this.ctx.save()
        this.ctx.scale(scale.x, scale.y)
        this.ctx.translate(pos.x, pos.y)
        this.ctx.globalAlpha = this.options.opacity ?? 1
        this.ctx.fillStyle = fillStyle
        this.ctx.fill(path)
        this.ctx.restore()
    }

    render() {
        if (this._path) this.fill(this.path, this.pos, this.colour.fg)
    }

    drawVertexSet(vectorSet: Point[][], translate?: Point, fillColour?: string, opacity = 1.0) {
        this.ctx.save()
        this.ctx.beginPath()
        this.ctx.fillStyle = fillColour ?? 'orange'
        this.ctx.globalAlpha = opacity
        if (translate) this.ctx.translate(translate.x, translate.y)
        for (let poly of vectorSet) {
            const { x, y } = poly[0]
            this.ctx.moveTo(x, y)
            for (let point of poly) {
                this.ctx.lineTo(point.x, point.y)
            }
            this.ctx.closePath()
            this.ctx.fill()
        }
        this.ctx.restore()
    }
}
