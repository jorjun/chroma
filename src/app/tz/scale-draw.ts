import { ElementRef } from '@angular/core'
import { Observable } from 'rxjs'
import { Draw, Options } from './base-draw'

export class ScaleDraw extends Draw {
    tick = {
        count: 13,
        heightSmall: 20,
        heightLarge: 10,
    }
    constructor(
        canvas: ElementRef,
        cursorState$: Observable<number>,
        obsFrame: Observable<number>,
        options?: Options
    ) {
        super(canvas, options)
        this.options = { colour: { background: 'pink', fg: 'gray' } }
    }
}
