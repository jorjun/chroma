// @jorjun V:vii ☉ ♎  ☽ ♉

import { ElementRef } from '@angular/core'
import { Observable } from 'rxjs'
import { first } from 'rxjs/operators'
import { Draw, Options } from './base-draw'

const SkyGradient = {
    4: '#192434', // Dark blue,
    7: '#2e5070', // Mid blue
    19: '#347ba4', // Blue
    22: '#2e5070', // Dark
}

function getGradient(hour: number) {
    const matchHour = Math.floor(hour + 1)
    const grad = [4, 7, 19, 22].filter((key) => {
        return key > matchHour
    })
    if (grad.length === 0) return SkyGradient[4]
    const idx = grad[0] as keyof typeof SkyGradient
    return SkyGradient[idx]
}

/** [---------CURSOR]    */
export class BackLeftDraw extends Draw {
    constructor(canvas: ElementRef, obsCursor: Observable<number>, obsFrame: Observable<number>, options?: Options) {
        super(canvas, options)
        obsCursor.subscribe((x) => {
            const box = new Path2D()
            box.rect(0, 0, x, this.height)
            this.path = box
            const hour = (x / this.width) * 24,
                fg = getGradient(hour)
            this.options = { colour: { background: 'gray', fg } }
        })
    }
}

/** [CURSOR---------]    */
export class BackRightDraw extends Draw {
    constructor(canvas: ElementRef, obsCursor: Observable<number>, obsFrame: Observable<number>, options?: Options) {
        super(canvas, options)
        this.options = { colour: { background: 'white', fg: '#F3F3F3' } }
        obsCursor.subscribe((x) => {
            const box = new Path2D()
            box.rect(x, 0, this.width - x, this.height)
            this.path = box
        })
    }
}
