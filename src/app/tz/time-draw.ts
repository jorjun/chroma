// @jorjun V:vii ☉ ♎  ☽ ♉

import { DateTime } from 'luxon'
import { ElementRef, Injectable } from '@angular/core'
import { BehaviorSubject, interval, Observable, timer, Subscription, Subject } from 'rxjs'
import { distinctUntilChanged, skip, take } from 'rxjs/operators'
import { Draw, Options } from './base-draw'

import { BackLeftDraw, BackRightDraw } from './background-draw'
import { LeftArrow, RightArrow } from './cursor-draw'

import { Legend, Side } from '@lib/text'
import { TimerService } from '@app/timer.service'

export interface TimeZone {
    name: string
    tz: string
    startTime: DateTime
}

export class TimeDraw extends Draw {
    leftOpacity = 1
    localTime = DateTime.local({ zone: this.timeZone.tz })
    legend = {
        city: new Legend('Berlin', 1, 18, this.ctx),
        time: new Legend('12:09', 2, 24, this.ctx),
        offset: new Legend('GMT+1', 3, 16, this.ctx),
    }
    showLeft = new BehaviorSubject(true)
    background: { [ref: string]: Draw } = {}
    anim?: Subscription
    renderSrc = new BehaviorSubject(0)

    constructor(
        canvas: ElementRef,
        private timeZone: TimeZone,
        cursor$: Observable<number>,
        private frame$: Observable<number>,
        private timerService: TimerService,
        options?: Options
    ) {
        super(canvas, options)
        this.background = {
            backLeft: new BackLeftDraw(canvas, this.renderSrc, frame$),
            backRight: new BackRightDraw(canvas, this.renderSrc, frame$),
            lArrow: new LeftArrow(canvas, this.renderSrc, frame$),
            rArrow: new RightArrow(canvas, this.renderSrc, frame$),
        }

        this.options = { colour: { background: 'gray', fg: 'black' } }
        this.legend.city.words = timeZone.name

        // Slide position=> set time display => trigger service time observer
        cursor$.subscribe((x) => {
            this._cursor.x = x
            this.localTime = this.cursorToTime(x)
            const utc = this.localTime.setZone('utc')
            this.timerService.updateTime(utc)
            this.legend.time.words = this.localTime.toFormat('HH:mm')
            const hours = Math.floor(this.localTime.offset / 60),
                mins = this.localTime.offset % 60,
                minutes = mins > 0 ? `:${mins}` : '',
                sign = Math.sign(hours) >= 0 ? '+' : '-'
            this.legend.offset.words = `GMT${sign}${Math.abs(hours)}${minutes}`
            this.renderSrc.next(x)
            this.showLeft.next(this.legend.city.isVisible(x)) // Trigger legend position switch (left/right)
            this.frame$.subscribe(() => this.renderSrc.next(x))
        })
        // Slider near an edge => Opacity transition for the legend-swap
        const frames = 6
        const fade = interval(40).pipe(take(frames))
        this.showLeft.pipe(distinctUntilChanged(), skip(1)).subscribe((showLeft) => {
            this.leftOpacity = showLeft ? 0 : 1
            this.anim?.unsubscribe()
            const delta = (showLeft ? 1 : -1) / frames
            this.anim = fade.subscribe(() => {
                this.leftOpacity += delta
                this.renderSrc.next(this._cursor.x)
            })
        })
        this.renderSrc.subscribe(() => this.render())
    }

    cursorToTime(x: number) {
        const interval15 = Math.floor((x / this.width) * 96),
            hour = Math.floor((x / this.width) * 24),
            minute = (interval15 * 15) % 60
        const localStart = this.timeZone.startTime.setZone(this.timeZone.tz)
        return this.localTime.set({ day: localStart.day, hour: hour, minute: minute, second: 0 })
    }

    render() {
        this.background.backLeft.clearRect()
        for (const draw of Object.values(this.background)) draw.render()

        const { x: cursorX } = this._cursor,
            { city, time, offset } = this.legend
        for (let side of [Side.Left, Side.Right]) {
            // if (side === Side.Right && cursorX > 200) continue
            for (let legend of [city, time, offset]) {
                legend.draw(side, cursorX, this.leftOpacity, this)
            }
        }
    }
}
