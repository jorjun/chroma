import { ElementRef } from '@angular/core'
import { normalisePath } from '@lib/utils'
import { Observable, pipe } from 'rxjs'
import { distinctUntilChanged, first } from 'rxjs/operators'

import { Draw, Options } from './base-draw'

export class LeftArrow extends Draw {
    constructor(
        canvas: ElementRef,
        cursorState$: Observable<number>,
        obsFrame: Observable<number>,
        options?: Options
    ) {
        super(canvas, options)
        this.options = { colour: { background: 'black', fg: 'white' } }
        this.path = normalisePath('M0,42 L-7,36 L0,31 L-2,30 L-12,36 L-2,43 L0,42')
        this.scale = 1.3
        this._cursor.y = 42
        cursorState$.subscribe((x) => {
            pipe(distinctUntilChanged())
            this._cursor.x = x - 32
        })
    }
}

export class RightArrow extends Draw {
    constructor(
        canvas: ElementRef,
        cursorState$: Observable<number>,
        obsFrame: Observable<number>,
        options?: Options
    ) {
        super(canvas, options)
        this.options = { colour: { background: 'gray', fg: 'black' } }
        this.path = normalisePath(
            'M11.944,56.287 L19.012,50.961 L11.944,45.636 L14.12,44 L23.379,50.961 L14.12,57.923 L11.944,56.287'
        )
        this.scale = 1.3
        this._cursor.y = 42
        cursorState$.subscribe((x) => {
            this._cursor.x = x + 33
        })
    }
}
