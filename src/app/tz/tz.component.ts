// @jorjun V:vii ☉ ♏  ☽ ♌

import { ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core'

import { Draw } from './base-draw'
import { TimeDraw, TimeZone } from './time-draw'
import { BehaviorSubject, interval, merge, Subject } from 'rxjs'
import { distinctUntilChanged, first, map, scan, share, takeUntil, throttleTime } from 'rxjs/operators'
import { animationFrameScheduler } from 'rxjs'

import { DateTime } from 'luxon'
import { TimerService } from '@app/timer.service'

interface Scene {
    [name: string]: Draw
}

@Component({
    selector: 'app-tz',
    templateUrl: './tz.component.html',
    styleUrls: ['./tz.component.sass'],
})
export class TzComponent implements OnInit, OnDestroy {
    @ViewChild('background', { static: true }) scrollBackRef!: ElementRef<HTMLCanvasElement>
    @Input() set name(name: string) {
        this.timeZone.name = name
    }
    @Input() set tz(tz: string) {
        this.timeZone.tz = tz
    }
    @Input() set time(time: DateTime) {
        this.selectedTime = time
    }
    @Input() set date(date: DateTime) {
        this.timeZone.startTime = date.setZone(this.timeZone.tz)
    }
    selectedTime = DateTime.utc()
    timeZone: TimeZone = {
        name: 'name here',
        tz: 'Europe/London',
        startTime: DateTime.utc(),
    }
    ngUnsubscribe = new Subject<void>()
    animationFrame$ = interval(0, animationFrameScheduler)
    cursorSrc = new BehaviorSubject(300)
    mouseState$ = new BehaviorSubject(false)
    dragMode = false

    asset: Scene = {}

    constructor(private timerService: TimerService, private cdr: ChangeDetectorRef) {
        this.cdr.detach()
    }

    mouseMove(evt: MouseEvent) {
        if (!this.dragMode) return
        const { movementX: x } = evt
        this.cursorSrc.next(x)
    }
    touchMove(evt: TouchEvent) {
        evt.preventDefault()
        const touch = evt.touches[0],
            { clientX } = touch,
            { left } = this.scrollBackRef.nativeElement.getBoundingClientRect()
        this.cursorSrc.next(clientX - left)
    }
    mouse = (action: 'up' | 'down') => this.mouseState$.next(action === 'down')

    ngOnInit() {
        this.mouseState$.subscribe((mode) => (this.dragMode = mode))
        const utc$ = this.timerService.utc$.pipe(
            takeUntil(this.ngUnsubscribe),
            map((dateTime) => this.timeToCursor(dateTime))
        )
        const cursorX$ = this.cursorSrc.pipe(
            takeUntil(this.ngUnsubscribe),
            scan((current: number, x: number) => x + current, 0),
            map((pos) => {
                const x = Math.min(this.pixelWidth, Math.max(0, pos))
                return x
            })
        )
        const watchCsr$ = merge(utc$, cursorX$).pipe(distinctUntilChanged(), throttleTime(20))
        const obsFrame = this.animationFrame$.pipe(takeUntil(this.ngUnsubscribe), first(), share())
        this.asset = {
            timeDraw: new TimeDraw(this.scrollBackRef, this.timeZone, watchCsr$, obsFrame, this.timerService),
        }
    }

    timeToCursor(utc: DateTime) {
        const localTime = utc.setZone(this.timeZone.tz)
        const midnight = DateTime.fromObject(
                { day: localTime.day, hour: 0, minute: 0, second: 0 },
                { zone: this.timeZone.tz }
            ),
            elapsedSec = localTime.toSeconds() - midnight.toSeconds(),
            prop = elapsedSec / (24 * 60 * 60)
        return Math.floor(prop * this.pixelWidth)
    }

    get pixelWidth() {
        return this.scrollBackRef.nativeElement.width
    }
    ngOnDestroy() {
        this.ngUnsubscribe.next()
        this.ngUnsubscribe.complete()
    }
}
