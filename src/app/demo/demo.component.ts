// @jorjun  V:vii ☉ ♎  ☽ ♎

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { MatSlider, MatSliderChange } from '@angular/material/slider'

import { processChroma, CanvasKind } from '@lib/luma_chroma_util'
import { Point } from '@lib/matrix'
import { BehaviorSubject, timer } from 'rxjs'
import { takeWhile } from 'rxjs/operators'

import { salesman } from '@lib/formula'
import { RandomPoly } from '@lib/random-poly'
import { DateTime } from 'luxon'

type CanvasRef = ElementRef<HTMLCanvasElement>

interface Canvas {
    [CanvasKind.source]: HTMLCanvasElement
    [CanvasKind.blue]: HTMLCanvasElement
}

@Component({
    selector: 'app-demo',
    templateUrl: './demo.component.html',
    styleUrls: ['./demo.component.sass'],
})
export class DemoComponent implements OnInit {
    @ViewChild('srcCanvas', { static: true }) srcCanvas!: CanvasRef
    @ViewChild('lumaCanvas', { static: true }) lumaCanvas!: CanvasRef
    @ViewChild('blueCanvas', { static: true }) blueCanvas!: CanvasRef
    @ViewChild('resultCanvas', { static: true }) resultCanvas!: CanvasRef
    // @ViewChild('lumaSlider', { static: true }) lumaSlider!: MatSlider
    @ViewChild('blueSlider', { static: true }) blueSlider!: MatSlider
    // ctxSrc!: CanvasRenderingContext2D
    canvas!: Canvas
    srcImg = new Image()
    fr = new FileReader()
    lowestCost = Number.MAX_SAFE_INTEGER // Guarantee finding better on first go
    selectedUTC = DateTime.utc()
    modifier = {
        // [CanvasKind.luma]: new BehaviorSubject<number>(0),
        [CanvasKind.blue]: new BehaviorSubject<number>(0),
    }
    points: Point[] = []
    tiles: RandomPoly[] = []

    constructor() {
        const poly = new RandomPoly(5, 500)
        this.points = poly.points
        for (let ix = 0; ix < 10; ix++) this.tiles.push(new RandomPoly(5, 20))
    }

    // User: filesystem image selection
    selectedImage(evt: any) {
        const localFile = evt.target!.files[0]
        this.fr.readAsDataURL(localFile)
        this.fr.onload = () => {
            const img = this.srcImg
            const dataUrl = this.fr.result?.toString()
            if (!dataUrl) throw `Bad data URL: ${dataUrl}`
            img.src = dataUrl
            img.onload = () => {
                const canvas = this.canvas[CanvasKind.source]
                const ctx = canvas.getContext('2d')!
                const { width, height } = img
                Object.keys(this.canvas).forEach((col) => {
                    this.canvas[col as CanvasKind].width = width
                    this.canvas[col as CanvasKind].height = height
                })
                ctx.drawImage(img, 0, 0)
                this.render()
            }
        }
    }

    sliderInput(event: MatSliderChange, col: 'blue') {
        if (event.value === null) throw `Bad event ${event}`
        this.modifier[col].next(event.value)
    }

    render() {
        const modifiers = {
            blue: this.modifier[CanvasKind.blue].value,
        }
        const result = processChroma(this.srcCanvas.nativeElement, modifiers)
        for (let col of [CanvasKind.blue]) {
            const canvas = this.canvas[col as CanvasKind]
            const ctx = canvas.getContext('2d')!
            const { height, width } = canvas
            const imgData = ctx.createImageData(width, height)
            const data = result[col as keyof typeof result]
            imgData.data.set(data)
            ctx.putImageData(imgData, 0, 0)
        }
    }

    playClick() {
        timer(0, 100)
            .pipe(takeWhile((count) => count < 1000))
            .subscribe((count) => {
                const { points, lowestCost } = salesman(this.points, this.lowestCost)
                this.points = points
                this.lowestCost = lowestCost
            })
    }

    ngOnInit(): void {
        this.canvas = {
            [CanvasKind.source]: this.srcCanvas.nativeElement,
            // [CanvasKind.luma]: this.lumaCanvas.nativeElement,
            [CanvasKind.blue]: this.blueCanvas.nativeElement,
        }
    }
}
