import { Injectable } from '@angular/core'
import { DateTime } from 'luxon'
import { BehaviorSubject } from 'rxjs'

@Injectable({
    providedIn: 'root',
})
export class TimerService {
    private _selectedTime = new BehaviorSubject(DateTime.utc())
    utc$ = this._selectedTime.asObservable()
    constructor() {}

    updateTime(time: DateTime) {
        this._selectedTime.next(time)
    }
}
