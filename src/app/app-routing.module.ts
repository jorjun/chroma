import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { PolygonComponent } from './polygon/polygon'
import { DemoComponent } from './demo/demo.component'
import { TzComponent } from './tz/tz.component'

const routes: Routes = [
    { path: '', component: TzComponent },
    { path: 'demo', component: DemoComponent },
    { path: 'tz', component: TzComponent },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
