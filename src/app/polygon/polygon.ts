// @jorjun V:vii ☉ ♎  ☽ ♍

import { Component, ElementRef, Input, ViewChild } from '@angular/core'

import { Point, randInt } from '@lib/matrix'

@Component({
    selector: 'app-polygon',
    templateUrl: './polygon.component.html',
    styleUrls: ['./polygon.component.sass'],
})
export class PolygonComponent {
    @ViewChild('canvas', { static: true }) canvas!: ElementRef<HTMLCanvasElement>
    @Input('points') set _points(points: Point[]) {
        this.points = points
        this.render()
    }
    points: Point[] = []

    ctx?: CanvasRenderingContext2D

    constructor() {
        // for (let ix of range(N)) this.nodes.push({ x: randInt(0, canvas.width), y: randInt(0, canvas.height) })
    }

    render(strokeStyle = 'black') {
        const ctx = this.ctx ?? this.canvas.nativeElement.getContext('2d')!,
            { width, height } = this.canvas.nativeElement,
            moveTo = (point: Point) => ctx.moveTo(point.x, point.y),
            lineTo = (point: Point) => ctx.lineTo(point.x, point.y)

        ctx.clearRect(0, 0, width, height)
        ctx.beginPath()
        ctx.strokeStyle = strokeStyle
        this.points.forEach((point, ix) => {
            if (ix === 0) moveTo(point)
            else lineTo(point)
        })
        ctx.closePath()
        ctx.stroke()
    }
}
