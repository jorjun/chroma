import { Point, randInt } from './matrix'

function sort(points: Point[]) {
    const N = points.length
    let p1 = points[randInt(0, N - 1)]
    points.sort((a, b) => cost(p1, b) - cost(p1, a))
}

function cost(a: Point, b: Point) {
    return Math.hypot(b.x - a.x, b.y - a.y)
}

function pathPost(points: Point[]) {
    const N = points.length
    return points.slice(0, -1).reduce((prv, current, ix) => prv + cost(current, points[(ix + 1) % N]), 0)
}

export function salesman(points: Point[], lowestCost: number) {
    const trialPath = [...points]
    const N = points.length
    let cost = 0
    for (let ix = 0; ix < N; ix++) {
        const nodeCount = randInt(3, N),
            node = randInt(0, N - nodeCount - 1),
            removed = trialPath.splice(node, nodeCount)
        sort(removed)
        trialPath.splice(node, 0, ...removed)
        cost = pathPost(trialPath)
        if (cost < lowestCost) break
    }
    if (cost < lowestCost) {
        points = [...trialPath]
        lowestCost = cost
    }
    return { points, lowestCost }
}
