// @jorjun V:vii ☉ ♎  ☽ ♏

import { Point, Tau } from './matrix'

/**
 * @param num1 from
 * @param num2 to
 * @returns iterator
 */
export function* range(num1: number, num2?: number, inc = 1) {
    const res: number[] = []
    if (!num2) {
        num2 = num1
        num1 = 0
    }
    for (let ix = num1; ix < num2; ix += inc) yield ix
}

export interface PathNode {
    c: 'M' | 'L'
    x: number
    y: number
}

export const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms)),
    /** Download SVG resource, process into vertex arrays */
    loadSvg = async (url: string, id = '', sampleCount = 30, scale = 1) =>
        fetch(url)
            .then((response) => response.text())
            .then((raw) => new window.DOMParser().parseFromString(raw, 'image/svg+xml'))
            .then((svgElement) => {
                const paths = svgElement.querySelectorAll('path')
                if (paths.length === 0) throw `Bad SVG: ${url}`
                const vertexSets = Array.from(paths)
                    .filter((el) => (id ? el.id === id : true))
                    .map((path) => {
                        const totalDist = path.getTotalLength(),
                            delta = totalDist / sampleCount,
                            points: Point[] = []
                        for (let dist = 0; dist < totalDist; dist += delta) {
                            const { x, y } = path.getPointAtLength(dist)
                            points.push({ x: x * scale, y: y * scale })
                        }
                        return points
                    })
                return vertexSets
            }),
    /** Rotate path elements (Used to place first node on screen-left: minimum `x`==>0 */
    rotate = <T>(array: T[], n: number) => {
        n = n % array.length
        return array.slice(n, array.length).concat(array.slice(0, n))
    },
    /** Grab x,y and c(ommand) array from SVG path string */
    pathSplit = (svgPath: string): PathNode[] =>
        svgPath.split(' ').map((pair) => {
            const [x, y] = pair.split(',')
            return { c: x[0] as PathNode['c'], x: parseFloat(x.slice(1)), y: parseFloat(y) }
        }),
    /** Take node array and return closed 2D path ready for drawing */
    pathToDraw = (svgPath: PathNode[]): Path2D => {
        const path2d = new Path2D()
        for (let node of svgPath) {
            switch (node.c) {
                case 'M':
                    path2d.moveTo(node.x, node.y)
                    break
                case 'L':
                    path2d.lineTo(node.x, node.y)
                    break
            }
        }
        path2d.closePath()
        return path2d
    },
    /** Find min X, order that node first; reduce all coordinates by this delta (translate path to zero-leftmost basis) */
    normalisePath = (svgPath: string): Path2D => {
        let path = pathSplit(svgPath)
        let min = {
            x: {
                i: -1, // path index
                v: 999999, // coordinate vertex
            },
            y: {
                i: -1,
                v: 999999,
            },
        }
        path.forEach((node, i) => {
            if (node.x < min.x.v) min.x = { i, v: node.x }
            if (node.y < min.y.v) min.y = { i, v: node.y }
        })
        // Translate path leftmost (minimum `x`=> 0)
        if (min.x.i > -1) {
            // `move` command should be in `min.x.i` place, not 0th (before rotation)
            if (path[0].c === 'M' && min.x.i > 0) [path[min.x.i].c, path[0].c] = [path[0].c, path[min.x.i].c]
            // Rotate minimum node.x into 0th place
            const pathLeft = rotate(path, min.x.i).map(({ x, y, c }) => {
                return { x: x - min.x.v, y, c }
            })
            path = pathLeft
        }
        // Translate every `y` so that minimum `y` ==> 0
        if (min.y.i > -1) {
            const pathUp = path.map(({ x, y, c }) => {
                return { x, y: y - min.y.v, c }
            })
            path = pathUp
        }
        return pathToDraw(path)
    },
    lerp = (vA: Point, vB: Point, prop = 0.5): Point => {
        return { x: vA.x + (vB.x - vA.x) * prop, y: vA.y + (vB.y - vA.y) * prop }
    }

export function* polygon(sides: number, scale: number) {
    const sep = Tau / sides
    const off = scale
    for (let ang = 0; ang < Tau; ang += sep) {
        yield { x: off + scale * Math.cos(ang + sep / 2), y: off + scale * Math.sin(ang + sep / 2) }
    }
}
