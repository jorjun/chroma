// @jorjun V:vii ☉ ♏  ☽ ♎

import { Draw } from '@app/tz/base-draw'

export enum Side {
    Left,
    Right,
}

/**  [New York]
      [12: 15]
    [GMT-:04:00]  */

export class Legend {
    vHeight = 28
    fontFace = 'sans-serif'
    font: string
    width: number

    constructor(
        private _words: string,
        private row: number,
        private readonly fontSize: number,
        private ctx: CanvasRenderingContext2D
    ) {
        this.font = `${this.fontSize}px ${this.fontFace}`
        this.width = this.getWidth()
    }

    set words(text: string) {
        this._words = text
        this.width = this.getWidth()
    }
    get words() {
        return this._words
    }
    getWidth() {
        const ctx = this.ctx
        ctx.save()
        ctx.font = this.font
        const { width } = ctx.measureText(this._words)
        ctx.restore()
        return width
    }
    get centre() {
        return this.width / 2
    }

    isVisible(cursorX: number) {
        return cursorX - this.centre - 110 > 0
    }
    layout(side: Side, cursorX: number, leftOpacity: number) {
        const y = this.vHeight * this.row
        switch (side) {
            case Side.Left:
                return { x: cursorX - 100, y, colour: 'white', opacity: leftOpacity }
            case Side.Right:
                return { x: cursorX + 100, y, colour: 'black', opacity: 1 - leftOpacity }
        }
    }
    draw(side: Side, cursorX: number, leftOpacity: number, draw: Draw) {
        const { x, y, colour, opacity } = this.layout(side, cursorX, leftOpacity)
        draw.drawText(this.words, { x, y }, colour, this.font, opacity)
    }
}
