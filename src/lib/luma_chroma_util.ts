import { Colour, setColour, YCbCrOffset, matrixYCbCr2RGB, subtract, multiply } from './matrix'

export enum CanvasKind {
    source = 'source',
    blue = 'blue',
}

interface LumaChromaModifiers {
    blue: number
    luma?: number
    red?: number
}

export const processChroma = (canvas: HTMLCanvasElement, modifiers: LumaChromaModifiers) => {
    const ctx = canvas.getContext('2d')!
    const srcData = ctx.getImageData(0, 0, canvas.width, canvas.height)

    const newImgData = () => new Uint8ClampedArray(srcData.data.length)
    const imgData = {
        // luma: newImgData(),
        blue: newImgData(),
        // result: newImgData(),
    }

    for (let ix = 0; ix < srcData.data.length; ix += 4) {
        let pixel = { r: srcData.data[ix], g: srcData.data[ix + 1], b: srcData.data[ix + 2] }
        const alpha = srcData.data[ix + 3]

        // const luma = new Colour(pixel, modifiers.luma, 235, 'r')
        // setColour(imgData.luma, ix, luma.channel, alpha)

        const blue = new Colour(pixel, modifiers.blue, 240, 'g')
        const blueChan = blue.channel
        const mono = blueChan.b
        // setColour(imgData.blue, ix, blue.channel, alpha)
        setColour(imgData.blue, ix, { r: mono, b: mono, g: mono }, alpha)

        // const vectorWithoutOffset = subtract({ r: luma.value, g: blue.value, b: 0 }, YCbCrOffset)
        // const rgb = multiply(matrixYCbCr2RGB, vectorWithoutOffset)
        // setColour(imgData.result, ix, rgb, alpha)
    }
    return imgData
}
