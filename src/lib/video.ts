// TODO : remove type: any ambiguities, they're fksin appalling

interface Frame {
    width: number
    height: number
    data: any
    riff: any
}

export class Video {
    canEncode() {
        const canvas = document.createElement('canvas')
        canvas.width = 8
        canvas.height = 8
        return canvas.toDataURL('image/webp', 0.1).indexOf('image/webp') > -1
    }
    constructor(public fps: number, public quality = 0.8, public name = 'webVideo') {
        if (!this.canEncode()) throw 'Unable to encode, try a different browser?'
        this.frameDelay = 1000 / fps
    }
    timecode = 0
    frame = 0
    webmData: any
    clusterTimecode = 0
    clusterCounter = 0
    CLUSTER_MAX_DURATION = 30000
    frameNumber = 0
    width?: number
    height?: number
    frameDelay: number
    videoMimeType = 'video/webm' // the only one.
    frameMimeType = 'image/webp' // can be no other
    stream = {
        num: (num: number) => {
            // writes int
            var parts = []
            while (num > 0) {
                parts.push(num & 0xff)
                num = num >> 8
            }
            return new Uint8Array(parts.reverse())
        },
        str: (str: string) => {
            // writes string
            var i, len, arr
            len = str.length
            arr = new Uint8Array(len)
            for (i = 0; i < len; i++) arr[i] = str.charCodeAt(i)
            return arr
        },
        compInt: (num: number) => {
            // could not find full details so bit of a guess
            if (num < 128) {
                // number is prefixed with a bit (1000 is on byte 0100 two, 0010 three and so on)
                num += 0x80
                return new Uint8Array([num])
            } else if (num < 0x4000) {
                num += 0x4000
                return new Uint8Array([num >> 8, num])
            } else if (num < 0x200000) {
                num += 0x200000
                return new Uint8Array([num >> 16, num >> 8, num])
            } else if (num < 0x10000000) {
                num += 0x10000000
                return new Uint8Array([num >> 24, num >> 16, num >> 8, num])
            } else return 0 // catch all?
        },
    }
    dataTypes: any = {
        object: (data: any) => this.toBlob(data),
        number: (data: any) => {
            return this.stream.num(data)
        },
        string: (data: any) => {
            return this.stream.str(data)
        },
        array: function (data: any) {
            return data
        },
        double2Str: (num: number) => {
            const S = String.fromCharCode
            var c = new Uint8Array(new Float64Array([num]).buffer)
            return S(c[7]) + S(c[6]) + S(c[5]) + S(c[4]) + S(c[3]) + S(c[2]) + S(c[1]) + S(c[0])
        },
    }
    ids = {
        // header names and values
        videoData: 0x1a45dfa3,
        Version: 0x4286,
        ReadVersion: 0x42f7,
        MaxIDLength: 0x42f2,
        MaxSizeLength: 0x42f3,
        DocType: 0x4282,
        DocTypeVersion: 0x4287,
        DocTypeReadVersion: 0x4285,
        Segment: 0x18538067,
        Info: 0x1549a966,
        TimecodeScale: 0x2ad7b1,
        MuxingApp: 0x4d80,
        WritingApp: 0x5741,
        Duration: 0x4489,
        Tracks: 0x1654ae6b,
        TrackEntry: 0xae,
        TrackNumber: 0xd7,
        TrackUID: 0x63c5,
        FlagLacing: 0x9c,
        Language: 0x22b59c,
        CodecID: 0x86,
        CodecName: 0x258688,
        TrackType: 0x83,
        Video: 0xe0,
        PixelWidth: 0xb0,
        PixelHeight: 0xba,
        Cluster: 0x1f43b675,
        Timecode: 0xe7,
        Frame: 0xa3,
        Keyframe: 0x9d012a,
        FrameBlock: 0x81,
    }
    keyframeD64Header = '\x9d\x01\x2a' //VP8 keyframe header 0x9d012a
    videoDataPos = 1 // data pos of frame data header
    defaultDelay = this.dataTypes.double2Str(1000 / 25)
    get header(): any {
        const ids = this.ids
        return [
            // structure of webM header/chunks what ever they are called.
            ids.videoData,
            [
                ids.Version,
                1,
                ids.ReadVersion,
                1,
                ids.MaxIDLength,
                4,
                ids.MaxSizeLength,
                8,
                ids.DocType,
                'webm',
                ids.DocTypeVersion,
                2,
                ids.DocTypeReadVersion,
                2,
            ],
            ids.Segment,
            [
                ids.Info,
                [ids.TimecodeScale, 1000000, ids.MuxingApp, 'WebVid', ids.WritingApp, 'WebVid', ids.Duration, 0],
                ids.Tracks,
                [
                    ids.TrackEntry,
                    [
                        ids.TrackNumber,
                        1,
                        ids.TrackUID,
                        1,
                        ids.FlagLacing,
                        0, // always o
                        ids.Language,
                        'und', // undefined I think this means
                        ids.CodecID,
                        'V_VP8', // These I think must not change
                        ids.CodecName,
                        'VP8', // These I think must not change
                        ids.TrackType,
                        1,
                        ids.Video,
                        [ids.PixelWidth, 0, ids.PixelHeight, 0],
                    ],
                ],
            ],
        ]
    }

    getHeader() {
        const header = this.header
        header[3][2][3] = name
        header[3][2][5] = name
        header[3][2][7] = this.dataTypes.double2Str(this.frameDelay)
        header[3][3][1][15][1] = this.width
        header[3][3][1][15][3] = this.height
        function create(dat: any) {
            var kv: any, data
            data = []
            for (let i = 0; i < dat.length; i += 2) {
                kv = { i: dat[i] }
                if (Array.isArray(dat[i + 1])) {
                    kv.d = create(dat[i + 1])
                } else {
                    kv.d = dat[i + 1]
                }
                data.push(kv)
            }
            return data
        }
        return create(header)
    }
    addCluster() {
        const ids = this.ids
        this.webmData[this.videoDataPos].d.push({
            i: ids.Cluster,
            d: [{ i: ids.Timecode, d: Math.round(this.clusterTimecode) }],
        }) // Fixed bug with Round
        this.clusterCounter = 0
    }

    // TODO define Frame type from example below..
    addFrame(frame: any) {
        const S = String.fromCharCode
        let VP8: any, kfS, riff: any
        riff = this.getWebPChunks(atob(frame.toDataURL(this.frameMimeType, this.quality).slice(23)))
        VP8 = riff.RIFF[0].WEBP[0]
        kfS = VP8.indexOf(this.keyframeD64Header) + 3
        frame = {
            width: ((VP8.charCodeAt(kfS + 1) << 8) | VP8.charCodeAt(kfS)) & 0x3fff,
            height: ((VP8.charCodeAt(kfS + 3) << 8) | VP8.charCodeAt(kfS + 2)) & 0x3fff,
            data: VP8,
            riff: riff,
        }
        if (this.clusterCounter > this.CLUSTER_MAX_DURATION) {
            this.addCluster()
        }
        this.webmData[this.videoDataPos].d[this.webmData[this.videoDataPos].d.length - 1].d.push({
            i: this.ids.Frame,
            d:
                S(this.ids.FrameBlock) +
                S(Math.round(this.clusterCounter) >> 8) +
                S(Math.round(this.clusterCounter) & 0xff) +
                S(128) +
                frame.data.slice(4),
        })
        this.clusterCounter += this.frameDelay
        this.clusterTimecode += this.frameDelay
        this.webmData[this.videoDataPos].d[0].d[3].d = this.dataTypes.double2Str(this.clusterTimecode)
    }
    startEncoding() {
        this.frameNumber = this.clusterCounter = this.clusterTimecode = 0
        this.webmData = this.getHeader()
        this.addCluster()
    }

    toBlob(vidData: any): Blob {
        var data: any, vData: any, len: number
        vData = []
        for (let i = 0; i < vidData.length; i++) {
            data = this.dataTypes[typeof vidData[i].d](vidData[i].d)
            len = data.size || data.byteLength || data.length
            vData.push(this.stream.num(vidData[i].i))
            vData.push(this.stream.compInt(len))
            vData.push(data)
        }
        return new Blob(vData, { type: this.videoMimeType })
    }

    getWebPChunks(str: string) {
        var offset: number,
            len: number,
            id = ''
        offset = 0
        let chunks: {
            [id: string]: string[]
        } = {}
        while (offset < str.length) {
            id = str.substr(offset, 4)
            // value will have top bit on (bit 32) so not simply a bitwise operation
            // (Will not work on big endian)
            len = new Uint32Array(
                new Uint8Array([
                    str.charCodeAt(offset + 7),
                    str.charCodeAt(offset + 6),
                    str.charCodeAt(offset + 5),
                    str.charCodeAt(offset + 4),
                ]).buffer
            )[0]
            id = str.substr(offset, 4)
            if (chunks[id] === undefined) chunks[id] = []
            if (id === 'RIFF' || id === 'LIST') {
                for (let chunk of this.getWebPChunks(str.substr(offset + 8, len))) chunks[id].push(chunk)
                offset += 8 + len
            } else if (id === 'WEBP') {
                chunks[id].push(str.substr(offset + 8))
                break
            } else {
                chunks[id].push(str.substr(offset + 4))
                break
            }
        }
        return chunks[id]
    }

    addFrame2(frame: any) {
        if ('canvas' in frame) frame = frame.canvas

        if (this.width === null) {
            this.width = frame.width
            this.height = frame.height
            this.startEncoding()
        } else if (this.width !== frame.width || this.height !== frame.height)
            throw RangeError('Frame size error. Frames must be the same size.')
        this.addFrame(frame)
        frame += 1
        this.timecode = this.clusterTimecode
    }
}
