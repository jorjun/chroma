import { Point } from './matrix'
import { lerp, polygon } from './utils'

export class RandomPoly2 {
    points: Point[]

    constructor(private num: number, private scale: number) {
        this.points = Array<Point>(num)
        const points = this.points
        let xPool = Array<number>(num),
            yPool = Array<number>(num)

        for (let i = 0; i < num; i++) {
            xPool[i] = Math.round(Math.random() * scale)
            yPool[i] = Math.round(Math.random() * scale)
        }

        xPool.sort((a, b) => a - b)
        yPool.sort((a, b) => a - b)

        let minX = xPool[0],
            minY = yPool[0],
            maxX = xPool[num - 1],
            maxY = yPool[num - 1]

        // Divide the interior points into two chains & Extract the vector components
        let xVec = Array<number>(num),
            yVec = Array<number>(num),
            lastMin = minX,
            lastMax = minX

        for (let i = 0; i < num - 2; i++) {
            const x = xPool[i]

            if (Math.random() > 0.5) {
                xVec[i] = x - lastMin
                lastMin = x
            } else {
                xVec[i] = lastMax - x
                lastMax = x
            }
        }

        xVec[num - 2] = maxX - lastMin
        xVec[num - 1] = lastMax - maxX

        lastMin = minY
        lastMax = minY

        for (let i = 0; i < num - 2; i++) {
            const y = yPool[i]
            if (Math.random() > 0.5) {
                yVec[i] = y - lastMin
                lastMin = y
            } else {
                yVec[i] = lastMax - y
                lastMax = y
            }
        }
        yVec[num - 2] = maxY - lastMin
        yVec[num - 1] = lastMax - maxY

        // Randomly pair up the X- and Y-components
        for (let i = 0; i < num; i++) {
            const dest = Math.floor(Math.random() * i)
            ;[yVec[i], yVec[dest]] = [yVec[dest], yVec[i]]
        }

        // Pairs into vectors
        let vec = Array<Point>(num)
        for (let i = 0; i < num; i++) vec[i] = { x: xVec[i], y: yVec[i] }
        vec.sort((a, b) => Math.atan2(a.x, a.y) - Math.atan2(b.x, b.y))

        // Find polygon width, height
        let x = 0,
            y = 0,
            minPolygonX = 0,
            minPolygonY = 0

        for (let i = 0; i < num; i++) {
            points[i] = { x, y }
            x += vec[i].x
            y += vec[i].y

            minPolygonX = Math.min(minPolygonX, x)
            minPolygonY = Math.min(minPolygonY, y)
        }

        // Shift polygon to origin
        let xShift = minX - minPolygonX,
            yShift = minY - minPolygonY

        for (let point of this.points) {
            point.x += xShift
            point.y += yShift
        }
    }
}

export class RandomPoly {
    points: Point[]

    constructor(private num: number, private scale: number) {
        let ix = 0
        const points = Array.from(polygon(num, scale))
        for (let p of points) {
            const p2 = points[ix + 1]
            if (ix < num - 1) points[ix] = lerp(p, p2, Math.random())
            else points[ix] = lerp(p, points[0], Math.min(0.2 + Math.random(), 0.8))
            ix++
        }
        this.points = points
    }
}
